# Component: Backend Partner

The component is made for being a parent of all backend components and services.

## Packaging

pom

## Description

The project contains tools used for QA like:
- Jacoco (with default required limit of code coverage by test)
- Spotbogs
- PMD
- Checkstyle and default configuration

and plugins for a compilation and a creating releases like:
- maven-deploy-plugin
- maven-release-plugin (including the configuration of scm to push new snapshot version and maven repository to push
  new versions of artifacts)
- maven-compiler-plugin (including the configuration for lombok and mapstruct libraries)

## Creating local release

```
mvn clean install
```

## Maven
```
    <parent>
         <groupId>com.jkarkoszka.getjabbed</groupId>
        <artifactId>backend-parent</artifactId>
        <version>1.0.7-SNAPSHOT</version>
    </parent>
```


## Access to GetJabbed Maven Repository

### settings.xml

```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                      http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <activeProfiles>
        <activeProfile>main</activeProfile>
    </activeProfiles>

    <profiles>
        <profile>
            <id>main</id>
            <repositories>
                <repository>
                    <id>central</id>
                    <url>https://repo1.maven.org/maven2</url>
                </repository>
                <repository>
                    <id>gitlab-maven</id>
                    <url>https://gitlab.com/api/v4/groups/51789812/-/packages/maven</url>
                </repository>
            </repositories>
        </profile>
    </profiles>

    <servers>
        <server>
            <id>gitlab-maven</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Private-Token</name>
                        <value>###PUT HERE PERSONAL ACCESS TOKEN###</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```

You can generate personal access token here: https://gitlab.com/-/profile/personal_access_tokens